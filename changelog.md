Changelog
====================================================================

v1.0
--------------------------------------------------------------------

### Modules

modules/marianne :
- ou comment intégrer la nouvelle _marque de l'État_ en respectant le RGAA

modules/block_link :
- précision dans la documentation

modules/select-a11y :
- correction d'anomalie dans la navigation au clavier (conflit entre les touches entrée et espace)


v0.9
--------------------------------------------------------------------

Gestion des vieux navigateurs modifiée : on ne gère plus les cas où ie<10.

### Modules

modules/block-link :
- _nouveau nom_ du module

modules/blockquote :
- moins de variables de personnalisation de la police et des bordures

modules/svg-icons :
- ajout de l'attribut _download_ pour les fichiers en téléchargement


v0.8
--------------------------------------------------------------------

### Modules

modules/collapse :
- moins de contraintes sur la structure html de l'élément à déplier
- amélioration des règles d'impression

modules/menu-simple :
- identification de l'item actif autrement que par les css seules, avec ajout de texte caché.

modules/select-a11y :
- mise à jour

modules/skip-content
- **nouveau module** : composant permettant d'afficher à la tabulation - et à la tabulation seulement - des liens pour éviter un contenu problématique (piège clavier, utilisation peu confortable pour les aides techniques, etc.)

modules/table :
- suppression des classes de présentation


v0.7
--------------------------------------------------------------------

Cette version constitue une étape intermédiaire vers une version 1.0.0

***changements majeurs*** : 
- plus aucun script ne nécessite jquery (concerne : anchor-focus, collapse, menu-simple, modal, skip-link, textarea-counter, u-comments, u-palette) ;
- les scripts des modules modal et collapse ont été réécrits
- nouveau module select-a11y, qui transforme un select (simple ou multiple) en liste de suggestions avec champ de recherche

### Modules

modules/buttons/buttons-color :
- correction des variations de couleurs qui présentaient des déclinaisons avec des contrastes insuffisants

modules/collapse :
- modifications pour permettre d'ouvrir tout par zones (ou tous les collapses de toute la page)

modules/lien-composite :
- nouveau module permettant de créer un lien composite accessible (par exemple composé d'une image, un titre, un chapeau)

modules/forms/forms-inline :
- amélioration de la présentation par défaut pour que les éléments ne soient pas tous collés les uns aux autres


v0.6.1
--------------------------------------------------------------------

Scampi est désormais également disponible en tant que module npm.

Quelques modifications ont été apportées pour faciliter l'utilisation sous cette forme, en particulier le renommage des fichiers principaux (ex. @import modules/alert/alert devient @import modules/alert/index).

La doc d'utilisation a été mise à jour (readme, doc sur le site pidila.gitlab.io)


v0.5
--------------------------------------------------------------------

### Core 

core/core-scampi-settings :
- remplacement de la font-stack 'sans-serif' par les system font (cf. http://markdotto.com/2018/02/07/github-system-fonts/ et https://css-tricks.com/snippets/css/system-font-stack/). 
- ajout de la fonction color-yiq pour le calcul des contrastes
- ajout de la variable $font-weight-normal pour réutilisation dans les modules

### Modules

modules/buttons :
- modularisation en module principal plus feuilles scss additionnelles pour maîtriser l'ajout de règles css en fonction du projet ;
- documentation et exemples.
- mise à jour du tableau des sous modules du module button

modules/forms :
- mise à jour du tableau des sous modules du module forms
- démo des module et sous modules en français

modules/alert :
- mise à jour de la documentation et ajout d'exemple


v0.4
--------------------------------------------------------------------

Suppression du répertoire \_tests, qui n'était pas utilisé. Déplacement du fichier récapitulatif scampi.scss qui y était présent vers la racine du dépôt.

### Core 

core/helpers : 
- définition d'une variable spécifique pour les paddings applicables aux éléments de class .container
- suppression du padding-top par défaut sur l'élément .main-container

### Modules

modules/browsehappy :
- un poil de décoration

modules/collapse :
- gestion d'un libellé sur deux lignes
- meilleur alignement des symboles ouvrir/fermer
- définitions de variables (gestion de la valeur, de la couleur, de la taille et du positionnement du picto, paddings du bouton et de la zone dépliable)

modules/forms
- regroupement de tous les modules dérivés et dépendants de forms dans modules/forms avec liste des partials à inclure en fonction du projet
- mise à jour des fragments twig 
- mise à jour de la doc


v0.3
--------------------------------------------------------------------

### Amélioration des modules, notamment sur le plan de l'accessibilité :

**Général :** 

Suppression du speak:none appliqué à des sélecteurs css (notamment lors d'ajouts de contenus décoratifs via :after ou :before) car cette règle n'est pas toujours bien respectée par les aides techniques. Lesdits contenus décoratifs sont désormais encapsulés (avec ou sans :before / :after) dans un span aria-hidden true.

**Modules :**

 - alert : création d'un nouveau bloc d'alerte (alert-emergency) contenant un pictogramme (phone.svg), avec variables associées. Introduction du picto par une macro (macro.svg). Gestion du bloc en flex-box.
 - blockquote : fix typo dans le mixin qui empêchait la prise en compte du font-style par défaut
 - breadcrumb : 
   - sémantique html, construction en ol/li
   - déplacement du séparateur sur un span aria-hidden true
 - buttons :
   - ajout de variables pour les styles par défaut des boutons (class .btn)
 - collapse : 
   - restauration de l'outline au focus qui avait disparu
   - placement du symbole dans un span aria-hidden=true
   - mise à jour du script en v1.8.0 
 - forms : 
    - ajout d'une class .form-duo pour des champs côte à côte
    - suppression de l'outline:none sur le focus des inputs
    - ajout de messages d'erreurs in situ
    - correction d'une variable érronée d'une media query
 - menu-simple : 
    - attribut aria-expanded true|false sur le bouton "hamburger"
    - amélioration cosmétique quand le libellé passe sur plusieurs lignes
    - wrapper jquery autour du script pour éviter les conflits avec les autres scripts
   - déplacement du séparateur sur un span aria-hidden true
 - modal :
    - bugfix: suppression du aria-hidden="true" quand la modale est ouverte
    - suppression du mixin opacity (montée des versions IE compat)
 - pagination : 
    - ajout d'un role="navigation" sur nav
    - remplacement de l'aria-label par un title pour "page précédente/suivante"
    - ajout d'un title explicite sur chaque numéro de page ("page 1" ...)
 - site-banner-simple : 
    - alt vide sur le logo
    - ajout d'une baseline
    - gestion du responsive des 3 éléments (logo, titre, baseline)
 - svg-icons : 
    - ajout de l'attribut focusable false pour correction de bug IE11
    - réglage à 1em de la taille par défaut
    - trois icônes livrées par défaut (lien externe, courriel, lien téléchargement) assorties de leur sprite
 - textarea-counter :
    - amélioration de la restitution du décompte par les aides techniques
 - u-comments : 
    - attribut aria-expanded true|false sur le bouton déclencheur
    - ajout d'un effet au focus et au hover sur le bouton déclencheur
 - u-debug :
    - simplification de l'affichage du rythme vertical

 ### Dépendances :

 Suppression du fichier jquery.min.js ; c'est désormais à l'utilisateur de choisir la version qui lui convient et de l'importer dans son projet. La documentation reflète ce nouveau process.

 ### Documentation :

 - Liens dans modules/readme vers les pages de doc/démo sur le site du PiDILA
 - Actualisation et rafraîchissement des fichiers md de presque chaque module
 - Création de doc pour des modules qui en manquaient


v0.2
--------------------------------------------------------------------

Ajout du changelog

Licences au format SPDX : (MIT + CECILL B)

editorconfig : on choisit l'indentation à deux espaces pour tous les fichiers

### core

Ajout du mixin opacity, qui sera notamment utile pour le module modal.

### ajout de modules

[svg-icons]
Module permettant d'utiliser un sprite svg pour les icônes ou pictogrammes avec des exemples d'utilisation et d'implémentation conformes au RGAA.

[modal]
Module permettant d'ouvrir une fenêtre modale. Base : Boostrap et Paypal + modifications permettant une pleine compatibilité RGAA.


### améliorations de modules

[textarea-counter]
Refactorisation et amélioration (merci Elise et Nicolas).

[pagination]
Refactorisation.

### fix
[menu-simple]
Les variables scss des modules doivent être déclarées en !default


v0.1.4
--------------------------------------------------------------------

Un logo pour scampi :)

Mention des licences dans l'entête de la css

### maj
[collapse]
Mise à jour des scripts et traduction en français
Correction des css et des exemple html

[forms]
Ajout de la classe .symbole-required + exemple d'utilisation

[anchor-focus]
Nettoyage, renommage, correction du chemin dans le fichier de test


v0.1.3
--------------------------------------------------------------------

Le nom du projet est « scampi-twig » plutot que « scampi-twig starter kit »

### core

Le script anchor_focus devient un module de scampi

Tous les fichiers markdowns ont un titre de niveau 1


v0.1.2
--------------------------------------------------------------------

Ajout du fichier package.json

Choix d'une double licence : MIT + CeCILL B.

### core
amélioration du .sr-only

### ajout

[Pagination]
Ajout du module

### revue
u-comments, table, button


v0.1.1
--------------------------------------------------------------------

### maj
[forms]
maj to boostrap v4 alpha4
ajout de la classe .fieldset-discrete

### revue
Revue des modules : browsehappy, site-banner, menu-simple, breadcrump, Adobeblank,rwd-util.


v0.1
--------------------------------------------------------------------

Ajout d'un numéro de version et ouverture publique du code de scampi.

Ajout d'une licence et énumération des licences des dépendances


Notes
--------------------------------------------------------------------

une commande fort utile :

git log --oneline --decorate --reverse v0.1.4..
