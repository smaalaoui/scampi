Scampi
=====================================================================

Scampi est un ensemble de composants sass/js/html accessibles et responsive développés à la DILA (Direction de l’information légale et administrative). Il vise à être utilisé en interne et par nos prestataires ou partenaires.

[Documentation et démos](https://pidila.gitlab.io/scampi/)

- - -

Table des matières
---------------------------------------------------------------------

1. [Contenu du dépôt](#contenu-du-d%C3%A9p%C3%B4t)
2. [Dépendances](#d%C3%A9pendances)
3. [Installation](#installation)
4. [Documentation](#documentation)
5. [Contributions](#contributions)
6. [Ressources tierces](#ressources-et-biblioth%C3%A8ques-tierces-parties-utilis%C3%A9es)
7. [Auteurs](#auteurs)
8. [License](#license)

- - -


Contenu du dépôt
---------------------------------------------------------------------

Répertoires de ce dépôt :

* **core/** : base sass pour tous les projets et sa documentation.
* **modules/** : composants additionnels (scss, js) et leur documentation.
* le fichier **scampi.scss** présente un "sommaire" des fichiers de la bibliothèque
* le présent README

Note : les modules préfixés '''u-''' sont destinés à la phase de développement et l’élaboration de styleguide.


Dépendances
---------------------------------------------------------------------

Scampi est développé à l’aide du préprocesseur Sass. Il vous faudra donc disposer d’un outil de compilation. Le pôle internet utilise la version [gulp-sass](https://www.npmjs.com/package/gulp-sass) de nodejs mais vous pouvez utiliser tout compilateur de votre choix.

Dans le cas d'utilisation de `gulp-sass`, ne pas oublier de le configurer pour accepter l'import depuis `node_modules` :

```js
.pipe(sass({
  ...
  includePaths: ['node_modules']
})
```


Installation
---------------------------------------------------------------------

Il existe trois méthodes pour utiliser Scampi dans un projet :


### En module npm ou yarn

Si votre projet comporte déjà des dépendances à des modules listés dans le fichier `package.json`.

```bash
  $ npm install @pidila/scampi
```

Remplacez la commande `npm` par `yarn` si vous le désirez.


### En submodule git

Scampi peut également être utilisé en déclarant un submodule au sein de votre dépôt. Attention en ce cas à ajuster les chemins d'import des fichiers.


### En téléchargeant Scampi

[Téléchargez](https://gitlab.com/pidila/scampi/repository/master/archive.zip) l’archive de Scampi et placez-la dans votre répertoire de styles. Cette méthode vous autorise à modifier ses fichiers pour votre propre usage mais en perdant l'opportunité de le mettre à jour automatiquement.


Documentation
---------------------------------------------------------------------

Sauf mention précisée dans sa documentation, chaque module est utilisable sans dépendre d’aucun autre et ne nécessite que le chargement du core.

Un fichier markdown est présent dans chaque module pour documenter son but et son usage. Lire les [principes d’utilisation et de personnalisation d’un module](modules/README.md).


Contributions
---------------------------------------------------------------------

Les contributions sont les bienvenues. La façon la plus simple est de forker ce dépôt puis de faire des pull request depuis celui-ci. Idéalement vous devriez consulter au préalable notre [guide du contributeur](https://gitlab.com/pidila/scampi/blob/master/CONTRIBUTING.md).

Si vous détectez une anomalie ou souhaitez proposer une évolution vous pouvez également [ouvrir un bug](https://gitlab.com/pidila/scampi/issues) sur le tracker de notre dépôt.

Le projet est versionné selon la [gestion sémantique de version](http://semver.org/lang/fr/).


Ressources et bibliothèques tierces parties utilisées
---------------------------------------------------------------------

| nom  | version | licence | url | informations complémentaires |
| :--- | :------ | :------ | :-- | :-----
| Adobe Blank | v1.045 | SIL OPEN FONT LICENSE Version 1.1 | https://github.com/adobe-fonts/adobe-blank | module adobe-blank
| Debugging Sass Maps | --- | --- | https://www.sitepoint.com/debugging-sass-maps/ | module u-debug
| Responsive-typography | --- | --- | https://github.com/liquidlight/responsive-typography | module rwd-utils
| Bootstrap | v4 | MIT License | https://github.com/twbs/bootstrap | modules alert, blockquote, buttons, forms, tables
| Svguse | V1.2.4 | MIT License | https://github.com/Keyamoon/svgxuse | 
| Feathericons | dev | MIT Licence | https://github.com/feathericons/feather 


Auteurs
---------------------------------------------------------------------

Pidila (Pôle intégration HTML de la DILA).

### Contact

pidila@dila.gouv.fr

#### Liste d’échanges et d’information :

[S’abonner](https://framalistes.org/sympa/subscribe/pidila-tools).


License
---------------------------------------------------------------------

Scampi est distribué sous une double licence : [MIT](https://gitlab.com/pidila/scampi/blob/master/LICENCE-MIT.md) et [CeCILL-B](http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html).

Vous pouvez utiliser Scampi avec l’une ou l’autre licence.

La documentation est sous licence [Creative Commons CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/fr/).

Note : cette page s’inspire de [https://github.com/edx/ux-pattern-library](https://github.com/edx/ux-pattern-library)
