# U-debug

Présentation
------------

Ce module est destiné à aider l'intégrateur pendant la phase de développement. Il est composé de trois utilitaires :

- debug-rwd
- debug-rythm
- debug-map

Utilisation
---------------------------------------------------------------------

### Debug rwd

Cet utilitaire permet d'afficher en bas à droite de la page le breakpoint en activité.

Pour le mettre en oeuvre, insérer en bas de style.scss :

```scss
$debug-rwd : true;
@import "@pidila/scampi/modules/u-debug/index";
```

### Debug rythm

```scss
@import "@pidila/scampi/modules/u-debug/index";

body {
  @include debug-rhythm(32);
}
```

La valeur entre parenthèses fixe l'écartement des lignes en pixels (par défaut, 24).

### Debug map

Cet utilitaire très spécifique n'est utile que lorsqu'on travaille sur des fonctions compliquées utilisant une map sass et qu'on a besoin de vérifier cette map.

Permet d'afficher le contenu d'une map sass, les niveaux et les correspondances clé/valeur. Si par exemple vous souhaitez en savoir plus sur la map des fontes :

Dans style.scss :

```scss
@import "@pidila/scampi/modules/u-debug/index";
@include debug-map($fonts-map);
```
La restitution de la map est à la fin du fichier style.css après compilation :

```css
@debug-map {
  ...
}
```
