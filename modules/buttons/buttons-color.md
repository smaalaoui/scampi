# Buttons-color


Présentation
---------------------------------------------------------------------

Ce module est dépendant du module <a href="buttons.html">buttons</a>.

Il permet de faire varier la couleurs des fonds et/ou des bordures des boutons avec des classes prédéfinies liées aux états d'alerte et variantes claire/foncée.



Accessibilité
---------------------------------------------------------------------

Le point le plus délicat pour l'accessibilité de ce sous-module est de veiller à respecter les valeurs de contraste entre le fond du bouton et son texte conformes aux normes d'accessibilité. Il serait opportun sur cet aspect d'anticiper le WCAG 2.1 qui fixe également des valeurs de contraste à respecter entre la bordure et le fond d'un bouton ([critère wcag 1.4.11](https://www.w3.org/WAI/standards-guidelines/wcag/new-in-21/#1411-non-text-contrast-aa).)


Utilisation
---------------------------------------------------------------------

Le module met à disposition les classes liées aux états d'alertes et messages : `.btn-success` `.btn-danger` `.btn-warning` `.btn-info`.

On peut compléter les classes prédéfinies ici par ce qu'on veut en utilisant le mixin `button-variant` qui accepte plusieurs paramètres :

* $background,
* $border, 
* $hover-background: valeur par défaut `darken($background, 7.5%)`, 
* $hover-border: valeur par défaut `darken($border, 15%)`, 
* $active-background: valeur par défaut `darken($background, 10%)`, 
* $active-border: valeur par défaut `darken($border, 12.5%)`.

Dans la pratique il suffira donc d'indiquer au mixin les valeurs de couleurs du background et du border. Ce mixin est d'ailleurs utilisé dans le module de base de buttons pour les class btn-primary et btn-secondary.

Exemple :

```scss
.mon-bouton-a-moi {
  @include button-variant(#ff00dd,#b3009b)
}
```


Exemples
---------------------------------------------------------------------

```html
<h3>Variantes avec classes prédéfinies</h3>
<p>
  <button type="button" class="btn btn-success">Succès</button>
  <button type="button" class="btn btn-danger">Danger</button>
  <button type="button" class="btn btn-warning">Attention</button>
  <button type="button" class="btn btn-info">Information</button>
</p>

<h3>Les mêmes en "outline"</h3>
<p>
  <button type="button" class="btn btn-outline-primary">Primaire</button>
  <button type="button" class="btn btn-outline-secondary">Secondaire</button>
  <button type="button" class="btn btn-outline-success">Succès</button>
  <button type="button" class="btn btn-outline-danger">Danger</button>
  <button type="button" class="btn btn-outline-warning">Attention</button>
  <button type="button" class="btn btn-outline-info">Information</button>
</p>
```
