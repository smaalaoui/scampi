# Buttons-size


Présentation
---------------------------------------------------------------------

Ce module est dépendant du module <a href="buttons.html">buttons</a>. 

Il permet de faire varier les tailles des boutons à l'aide du mixin button-size, qui accepte cinq paramètres :

* $padding-y, 
* $padding-x, 
* $font-size, 
* $line-height, 
* $border-radius.

De plus, si le setting $enable-rounded est à true dans les settings genéraux, un border-radius est appliqué.


Utilisation
---------------------------------------------------------------------

Deux classes prédéfinies sont disponibles :

* `.btn-lg` (gros boutons) ;
* `.btn-sm` (petits boutons).


Exemple
---------------------------------------------------------------------

```html
<p>
  <button type="button" class="btn btn-lg">Gros bouton</button>
  <button type="button" class="btn btn-sm">Petit button</button>
</p>
<p>On peut bien sûr combiner les classes de boutons :</p>
<p>
  <button type="button" class="btn btn-primary btn-sm">Petit bouton primaire</button>
  <button type="button" class="btn btn-secondary btn-lg">Gros bouton secondaire</button>
</p>
```
