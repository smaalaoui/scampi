# Buttons-group


Présentation
---------------------------------------------------------------------

Ce module est dépendant du module <a href="buttons.html">buttons</a>. Il permet de regrouper des boutons, par exemple pour donner au groupe l'aspect d'une barre d'outils.


Accessibilité
---------------------------------------------------------------------

Afin de restituer aux aides techniques l'information qu'il s'agit de boutons groupés, on veillera à donner au conteneur du groupe un attribut `role="group"` et un attribut `aria-label="intitulé du groupe"`.


Utilisation
---------------------------------------------------------------------

Appliquer la class btn-group à l'élément contenant les boutons. Ne pas oublier le role group et l'aria-label.

Comme son homologue d'origine, buttons-group est livré avec des classes additionnelles permettant de régler la taille ou les variantes de couleur. On peut également imbriquer les buttons-group, par exemple pour que l'un des éléments de premier niveau accueuille un "dropdown".


Exemple
---------------------------------------------------------------------

```html
<p class="btn-group" role="group" aria-label="Titre du groupe de boutons">
  <button type="button" class="btn btn-secondary">Gauche</button>
  <button type="button" class="btn btn-secondary">Bouton</button>
  <button type="button" class="btn btn-secondary">Bouton</button>
  <button type="button" class="btn btn-secondary">Droit</button>
</p>

```

<p>Plus d'exemples sur les différentes combinaisons et utilisations de btn-group dans la documentation du module homologue de Bootstrap, <a href="https://getbootstrap.com/docs/4.1/components/button-group/" target="_blank" title="Documentation du module Button Group de Bootstrap - Nouvelle fenêtre">Button group</a>.</p>
