// menu-simple
// thanks to http://www.a11ymatters.com/pattern/mobile-nav/

var Scampi = Scampi || {};

if(document.documentElement.classList.contains('no-js')){
  document.documentElement.classList.remove('no-js');
  document.documentElement.classList.add('js');
}

Scampi.menuSimple = function menuSimple(){
  var toggle = document.querySelector('#toggle-menu');
  var menu = document.querySelector('.nav-main-list');

  if(!toggle){
    return;
  }

  toggle.addEventListener('click', function(){
    if (menu.classList.contains('is-open')) {
      toggle.setAttribute('aria-expanded', 'false');
      menu.classList.remove('is-open');
    } else {
      menu.classList.add('is-open');
      toggle.setAttribute('aria-expanded', 'true');
    }
  });
};

Scampi.menuSimple();
