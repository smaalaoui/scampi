# Marianne

Présentation
-------------

Plus qu'un module, ce composant est un modèle permettant d'afficher le nouveau bloc marque de l'État et le logotype du service en étant conforme RGAA.

Le nouveau <em>bloc marque</em> met en avant le côté officiel . La mention <strong>République Française</strong> doit donc être retranscrite pour toutes et tous.


Utilisation
---------------------------------------------------------------------

Ce module nécessite le <a href="block-link.html">module block-link</a>.

Placer uniquement la partie signifiante du lien, celle qu'on souhaite voire restituée par les aides techniques, dans une balise `a`.

Pour que toute la zone soit cliquable on utilise le pseudo élément `:after` sur le lien pour l'étendre sur tout le bloc parent (en position relative), grâce à une position absolute.


### Accessibilité

- Proposer une alternative pertinente à l'image représentant la marianne : <em>République Française. Liberté Égalité Fraternité.</em>
- Donner la fonction du lien sur l'image-lien représentant le logotype du service.

Exemple
-------

``` html

<div class="logo block-link">
  <img src="marianne-rf.png" alt="République Française. Liberté, Égalité, Fraternité.">
  <a href="#"><img src="logotype.png" alt="Retour à l'accueil de mon site" /></a>
</div>

```
