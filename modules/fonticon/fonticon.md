# Fonticon

Présentation
------------

Ce module permet d'utiliser une fonte d'icônes. Il comporte l'import de la fonte et les règles css minimales pour la mise en œuvre.

À titre d'exemple, une fonte avec deux icônes (lien externe et lien de téléchargement) est fournie dans le répertoire du module. On peut constituer sa propre fonte d'icônes à partir de fichiers svg avec des services en ligne tels que icomoon (voir [cet article](http://www.aieaieeye.com/creer-ses-propres-font-icon-en-5-etapes/)).

Il est recommandé de l'utiliser conjointement avec le module [adobe-blank](adobe-blank.html) et de l'importer le plus tôt possible dans le fichier style.css pour la rendre disponible dès le début des autres règles.

Utilisation
-----------

### Configuration

La variable proposée dans ce module est :

- `$fonticon-path` : chemin vers le répertoire contenant les fonticons, sa valeur par défaut est `../webfonts/fonticon/`


### Accessibilité

Pour ne pas « polluer » la lecture de la page par les aides techniques, les icônes sont placées en css (avec la propriété :before) sur un span vide doté de l'attribut aria-hidden="true". 

Exemples d’utilisation
---------------------

```` html
<p><a href="#">
  <span class="icon icon-download" aria-hidden="true"></span> 
  Rapport d'activité 
  <span class="blank">Télécharger</span>
</a></p>

<p><a href="#">
  Un lien externe 
  <span class="blank">- Nouvelle fenêtre</span>
  <span class="icon icon-external" aria-hidden="true"></span>
</a></p>
````

À noter
-------

Pour les icônes le Pôle intégration html de la DILA utilise plus volontiers un sprite SVG et le module [svg-icons](svg-icons.html) associé à du texte masqué par la [class sr-only](core-mixins.html).
