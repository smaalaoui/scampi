/*******************
  color swatch
********************/

var Scampi = Scampi || {};

Scampi.uPalette = function uPalette(){
  var colors = [".sg-color-swatch", ".sg-color-swatch-lighten", ".sg-color-swatch-darken"];
  var reBG = /^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/;

  function hex(x) {
    return ("0" + parseInt(x).toString(16)).slice(-2);
  }

  function getColor(el){
    var bg = el.style.backgroundColor.length ? el.style.backgroundColor : window.getComputedStyle(el).getPropertyValue("background-color");

    if(bg.search("rgb") === -1){
      return bg;
    }

    bg = bg.match(reBG);

    return "#" + hex(bg[1]) + hex(bg[2]) + hex(bg[3]);
  }

  colors.forEach(function(type){
    var colorSwatches = document.querySelectorAll(type);

    if(!colorSwatches){
      return;
    }

    Array.prototype.forEach.call(colorSwatches, function(swatch){
      if(swatch.lastElementChild){
        swatch.lastElementChild.insertAdjacentHTML('afterend', '<p class="sg-color-swatch-hex">'+getColor(swatch)+'</p>')
      }
      else {
        swatch.innerText = getColor(swatch);
      }
    });
  });
}

Scampi.uPalette();
